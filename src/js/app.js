import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebP()

const menuBtn = document.querySelector('.menu-btn');

document.addEventListener('click', menu);
   
function menu(ev) {
    if(ev.target.closest('.menu-icon')){
        menuBtn.classList.toggle('active')
    }
    if(!ev.target.closest('.menu-btn') || ev.target.closest('.close')) {
        menuBtn.classList.remove('active')
    }
}